#include <EEPROM.h>

unsigned int addr = 0;
int val = 0;

void setup() 
{
  Serial.begin(9600);
  while(!Serial){
    ;
  }
  val = EEPROM.read(addr);
  val++;
  EEPROM.write(addr, val);
}

void loop() 
{
  Serial.println(val);
}